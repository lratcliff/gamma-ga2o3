This file contains the relation between the structure labels used in the corresponding paper and those used in this dataset.
Low Energy
2-site
IIA	1211
IIB	1071
IIC	1101
IID	1062
IIE	1201
3-site
IIIA	1051
IIIB	1021
IIIC	1042
IIID	1024
IIIE	932
4-site
IVA	1177
IVB	1118
IVC	1209
IVD	1078
IVE	1188
Random
2-site
IIa	1216
IIb	1124
3-site
IIIa	1009
IIIb	859
IIIc	1172
IIId	998
IIIe	225
IIIf	81
IIIg	241
4-site
IVa	1114
IVb	794
IVc	456
IVd	702
IVe	121
IVf	408
