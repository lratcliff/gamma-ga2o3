# Gamma-Ga2O3

This repository contains the Jupyter notebooks, accompanying code, and data required to fit the models to predict the energy of gamma-Ga2O3 structures. Also included is an example notebook demonstrating structure generation and the application of the model, the BigDFT input file used to calculate single point energies, and the relaxed structures. For further information, please see the associated publication at https://doi.org/10.1002/adma.202204217. 

## Dependencies

This project uses Scikit-learn (https://scikit-learn.org/stable/).

## Authors and acknowledgment

Code was written by Laura Ratcliff, in collaboration with the authors of the associated paper. For further acknowledgments, including funding and associated computer resources, please refer to the publication acknowledgments section.
