######################################################################################
# This file contains routines which are specific to Ga2O3:                           #
# - list of initial positions                                                        #
# - write_ga2o3                                                                      #           
# - read_ga2o3                                                                       #           
# - generate_ga2o3                                                                   #
# - get_all_sites                                                                    #
######################################################################################


# initial fractional coordinates for gamma-Ga2O3 taken from neutron diffraction data
# ref: Playford et al., Chem. Eur. J. 19, 2803 (2013)
initial_pos = {}
initial_pos['Ga1'] = [[0.125, 0.125, 0.125],
                      [0.875, 0.875, 0.875],
                      [0.625, 0.125, 0.625],
                      [0.375, 0.875, 0.375],
                      [0.125, 0.625, 0.625],
                      [0.875, 0.375, 0.375],
                      [0.625, 0.625, 0.125],
                      [0.375, 0.375, 0.875]]

initial_pos['Ga2'] = [[0.500, 0.500, 0.500],
                      [0.250, 0.750, 0.000],
                      [0.750, 0.250, 0.000],
                      [0.750, 0.000, 0.250],
                      [0.250, 0.000, 0.750],
                      [0.000, 0.250, 0.750],
                      [0.000, 0.750, 0.250],
                      [0.500, 0.000, 0.000],
                      [0.250, 0.250, 0.500],
                      [0.750, 0.750, 0.500],
                      [0.750, 0.500, 0.750],
                      [0.250, 0.500, 0.250],
                      [0.000, 0.500, 0.000],
                      [0.500, 0.250, 0.250],
                      [0.500, 0.750, 0.750],
                      [0.000, 0.000, 0.500]]

initial_pos['Ga3'] = [[0.368, 0.125, 0.125],
                      [0.632, 0.875, 0.875],
                      [0.382, 0.125, 0.625],
                      [0.618, 0.875, 0.375],
                      [0.882, 0.625, 0.625],
                      [0.118, 0.375, 0.375],
                      [0.868, 0.625, 0.125],
                      [0.132, 0.375, 0.875],
                      [0.125, 0.368, 0.125],
                      [0.875, 0.632, 0.875],
                      [0.625, 0.382, 0.125],
                      [0.375, 0.618, 0.875],
                      [0.625, 0.882, 0.625],
                      [0.375, 0.118, 0.375],
                      [0.125, 0.868, 0.625],
                      [0.875, 0.132, 0.375],
                      [0.125, 0.125, 0.368],
                      [0.875, 0.875, 0.632],
                      [0.125, 0.625, 0.382],
                      [0.875, 0.375, 0.618],
                      [0.625, 0.625, 0.882],
                      [0.375, 0.375, 0.118],
                      [0.625, 0.125, 0.868],
                      [0.375, 0.875, 0.132],
                      [0.875, 0.618, 0.375],
                      [0.125, 0.382, 0.625],
                      [0.375, 0.132, 0.875],
                      [0.625, 0.868, 0.125],
                      [0.132, 0.875, 0.375],
                      [0.868, 0.125, 0.625],
                      [0.618, 0.375, 0.875],
                      [0.382, 0.625, 0.125],
                      [0.875, 0.375, 0.132],
                      [0.125, 0.625, 0.868],
                      [0.375, 0.875, 0.618],
                      [0.625, 0.125, 0.382],
                      [0.368, 0.625, 0.625],
                      [0.632, 0.375, 0.375],
                      [0.882, 0.125, 0.125],
                      [0.118, 0.875, 0.875],
                      [0.125, 0.125, 0.882],
                      [0.875, 0.875, 0.118],
                      [0.625, 0.625, 0.368],
                      [0.375, 0.375, 0.632],
                      [0.875, 0.118, 0.875],
                      [0.125, 0.882, 0.125],
                      [0.375, 0.632, 0.375],
                      [0.625, 0.368, 0.625]]
    
initial_pos['Ga4'] = [[0.000, 0.000, 0.000],
                      [0.750, 0.250, 0.500],
                      [0.250, 0.750, 0.500],
                      [0.250, 0.500, 0.750],
                      [0.750, 0.500, 0.250],
                      [0.500, 0.750, 0.250],
                      [0.500, 0.250, 0.750],
                      [0.000, 0.500, 0.500],
                      [0.750, 0.750, 0.000],
                      [0.250, 0.250, 0.000],
                      [0.250, 0.000, 0.250],
                      [0.750, 0.000, 0.750],
                      [0.500, 0.000, 0.500],
                      [0.000, 0.750, 0.750],
                      [0.000, 0.250, 0.250],
                      [0.500, 0.500, 0.000]]

initial_pos['O'] = [[0.2552, 0.2552, 0.2552],
                    [0.7448, 0.7448, 0.7448],
                    [0.4948, 0.9948, 0.7552],
                    [0.5052, 0.0052, 0.2448],
                    [0.9948, 0.7552, 0.4948],
                    [0.0052, 0.2448, 0.5052],
                    [0.7552, 0.4948, 0.9948],
                    [0.2448, 0.5052, 0.0052],
                    [0.0052, 0.5052, 0.2448],
                    [0.9948, 0.4948, 0.7552],
                    [0.5052, 0.2448, 0.0052],
                    [0.4948, 0.7552, 0.9948],
                    [0.2448, 0.0052, 0.5052],
                    [0.7552, 0.9948, 0.4948],
                    [0.2552, 0.7552, 0.7552],
                    [0.7448, 0.2448, 0.2448],
                    [0.4948, 0.4948, 0.2552],
                    [0.5052, 0.5052, 0.7448],
                    [0.9948, 0.2552, 0.9948],
                    [0.0052, 0.7448, 0.0052],
                    [0.0052, 0.0052, 0.7448],
                    [0.9948, 0.9948, 0.2552],
                    [0.5052, 0.7448, 0.5052],
                    [0.4948, 0.2552, 0.4948],
                    [0.7552, 0.2552, 0.7552],
                    [0.2448, 0.7448, 0.2448],
                    [0.2552, 0.4948, 0.4948],
                    [0.7448, 0.5052, 0.5052],
                    [0.7448, 0.0052, 0.0052],
                    [0.2552, 0.9948, 0.9948],
                    [0.7552, 0.7552, 0.2552],
                    [0.2448, 0.2448, 0.7448]]


# Function to write periodic Ga2O3 structures, which are:
# in BigDFT .xyz format
# assumed to be orthorhombic
# have Ga sites listed as G1, G2 etc.
def write_ga2o3(filename, alat, ndims, n, nGa_filled, num_filled, num_all, Ga_sites, filled_sites):

    f= open(filename,"w+")

    # write the header, which includes some summary information
    f.write(str(n)+' angstroemd0\n')
    f.write('periodic  '+str(alat[0] * ndims[0])+' '+str(alat[1] * ndims[1])+'  '+str(alat[2] * ndims[2])+\
            '    #Ga'+str(nGa_filled)+'O'+str(num_filled['O'])+' ('\
            +str(float(num_filled['Ga1'])/num_all['Ga1'])+', '+str(float(num_filled['Ga2'])/num_all['Ga2'])+', '\
            +str(float(num_filled['Ga3'])/num_all['Ga3'])+', '+str(float(num_filled['Ga4'])/num_all['Ga4'])+')\n')

    for g,Ga_site in enumerate(Ga_sites):
        for Ga in filled_sites[Ga_site]:
            f.write('G'+str(g+1)+'  '+str(Ga[0])+'  '+str(Ga[1])+'  '+str(Ga[2])+'\n')
    for O in filled_sites['O']:
        f.write('O  '+str(O[0])+'  '+str(O[1])+'  '+str(O[2])+'\n')

    f.close()


# Function to read in periodic Ga2O3 structures, which are:
# in BigDFT .xyz format
# assumed to be orthorhombic
# have Ga sites listed as G1, G2 etc.
def read_ga2o3(filename):

    data = open(filename)
    pos = data.readlines()

    # positions where Ga atoms are labelled by type of Ga site
    site_posinp = []
    # positions where Ga atoms are labelled as Ga only
    posinp = []

    nO = 0
    nGa = [0, 0, 0, 0]
    nat = int(pos[0].split()[0])
    alat = [float(pos[1].split()[1]), float(pos[1].split()[2]), float(pos[1].split()[3])]
    for i in range(2, nat+2):
        at = pos[i].split()[0]
        x = float(pos[i].split()[1])
        y = float(pos[i].split()[2])
        z = float(pos[i].split()[3])

        site_posinp.append([at, x, y, z])

        if at[0] == 'G':
            atl = 'Ga'
        else:
            atl = 'O'
        posinp.append([atl, x, y, z])

        if at == 'O':
            nO += 1
        else:
            for g in range(4):
                if at == 'G'+str(g+1):
                    nGa[g] += 1

    nGa_all = sum(nGa)
    n = nO + nGa_all
    ncells = nO / 32
    # total number of available Ga sites of each type
    nGa_tot = [ncells * 8, ncells * 16, ncells * 48, ncells * 16]

    nsite = 0
    for i in range(4):
        if nGa[i] > 0:
            nsite += 1

    return {'posinp': posinp, 'site_posinp': site_posinp, 'alat': alat, 'nat': n, 'nat_O': nO, 'nat_Ga': nGa_all, 'nsite': nsite,
            'occupancies': [float(nGa[0])/nGa_tot[0],float(nGa[1])/nGa_tot[1], float(nGa[2])/nGa_tot[2], float(nGa[3])/nGa_tot[3]]}


# Function to generate a structure of Ga2O3, occupying Ga sites according to a given probability
def generate_ga2o3(name, filename, alat, ndims, occupancies, mindist_GaGa=0.0, mindist_GaO=0.0, rnoise=0.0, impose_stoich=True, max_attempts=1000000):
    import numpy as np
    import copy
    import random
    import model_setup

    species = ['Ga1', 'Ga2', 'Ga3', 'Ga4', 'O']
    all_sites = get_all_sites(alat, ndims, rnoise) 

    num_filled = {}
    num_avail = {}
    num_all = {}
    filled_sites = {}
    empty_sites = {}
    avail_sites = {}
    for spec in species:
        filled_sites[spec] = []
        num_filled[spec] = len(filled_sites[spec])

        empty_sites[spec] = copy.deepcopy(all_sites[spec])
        avail_sites[spec] = copy.deepcopy(all_sites[spec])
        num_avail[spec] = len(avail_sites[spec])

        num_all[spec] = len(all_sites[spec])

    # fill O atoms first - in this case just take all atoms 
    spec = 'O'
    filled_sites['O'] = copy.deepcopy(all_sites['O'])
    num_filled[spec] = len(filled_sites[spec])
    del empty_sites[spec][:]
    del avail_sites[spec][:]

    # now fill Ga atoms
    # first work out how many Ga atoms we want
    nGa_stoich = np.ceil(2.0 * num_filled['O'] / 3.0)

    # Ga site-specific arrays
    Ga_sites = ['Ga'+str(i) for i in range(1, 5)]
    # find total number of Ga sites
    nGa_total = 0
    for Ga_site in Ga_sites:
        nGa_total += len(all_sites[Ga_site])

    # limit the number of attempts to add Ga atoms, to avoid any chance of an infinite loop
    attempts = 0
    while attempts < max_attempts:
        # check how many atoms we have of each site type in both the filled and empty site lists
        nGa_avail = 0
        nGa_filled = 0
        for Ga_site in Ga_sites:
            num_avail[Ga_site] = len(avail_sites[Ga_site])
            num_filled[Ga_site] = len(filled_sites[Ga_site])
            nGa_avail += num_avail[Ga_site]
            nGa_filled += num_filled[Ga_site]
                    
        # exit if we have enough atoms to be stoichiometric and are imposing stoichiometry
        if nGa_filled == nGa_stoich and impose_stoich:
            break
        # otherwise exit if we have run out of atoms to try and place
        elif nGa_avail == 0:
            attempts = max_attempts
            break
          
        # pick type of Ga atom site (weighted by number of those types of sites)
        i = random.randint(1, nGa_total)
        ic = 1
        for Ga_site in Ga_sites:
            if i >= ic and i < ic + len(all_sites[Ga_site]):
                Ga_type = Ga_site
                break
            ic += len(all_sites[Ga_site])

        # pick atom within the remaining atoms of that type
        # if there are none left, try again
        if num_avail[Ga_type] == 0:
            attempts += 1
            continue
        Ga_at = random.randint(0, num_avail[Ga_type] - 1)

        # check whether this is too close to existing atoms 
        too_close = False
        for spec in species:
            for at in filled_sites[spec]:
                d = model_setup.at_dist(at, avail_sites[Ga_type][Ga_at], [ndims[i] * alat[i] for i in range(3)])
                if spec[0:1] == 'G' and d < mindist_GaGa:
                    too_close = True
                elif spec[0:1] == 'O' and d < mindist_GaO:
                    too_close = True
            
        if too_close:
            # no point checking this one again, so we can remove it from available, but keep in empty for future use
            remove = avail_sites[Ga_type].pop(Ga_at)
            attempts += 1
            continue

        r = 0.0001 * random.randint(0, 10001)
        if r < occupancies[Ga_type]:
            filled_sites[Ga_type].append(avail_sites[Ga_type][Ga_at])
            # remove from both available and empty
            remove = empty_sites[Ga_type].pop(Ga_at)
            remove = avail_sites[Ga_type].pop(Ga_at)

    n = nGa_filled + num_filled['O']

    # we exceeded the number of allowed failed attempts
    if attempts == max_attempts:
        success = False
    else:  
        success = True
        write_ga2o3(filename, alat, ndims, n, nGa_filled, num_filled, num_all, Ga_sites, filled_sites)

    return {'success': success, 'alat': alat, 'supercell': ndims, 'N': n, 'NO': num_filled['O'], 'NGa': nGa_filled,
            'num_filled': num_filled, 'all_sites': all_sites, 'empty_sites': empty_sites, 'filled_sites': filled_sites}


# Function to generate a complete list of possible sites that could be occupied using neutron diffraction data as a starting point
def get_all_sites(alat, ndims, rnoise=0.0):
    import random 

    all_sites = {}
    species = ['Ga1', 'Ga2', 'Ga3', 'Ga4', 'O']
    for spec in species:
        all_sites[spec] = [] 

    # loop over site type, atom index and supercell indices
    for ix in range(ndims[0]):
        for iy in range(ndims[1]):
            for iz in range(ndims[2]):
                for spec in species:
                    for at in initial_pos[spec]:
                        rx = random.uniform(-rnoise, rnoise)
                        ry = random.uniform(-rnoise, rnoise)
                        rz = random.uniform(-rnoise, rnoise)
                        all_sites[spec].append([(at[0] + ix) * alat[0] + rx, (at[1] + iy) * alat[1] + ry, (at[2] + iz) * alat[2] + rz])

    return all_sites
