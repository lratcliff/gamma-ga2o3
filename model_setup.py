######################################################################################
# This file contains routines for setting up the machine learning model:             #
# - find_neighbours                                                                  #           
# - write_reference_environments                                                     #
# - read_reference_environments                                                      #
# - at_dist                                                                          #
# - deconstruct_structure                                                            #
# - write_decomposition                                                              #
# - read_decomposition                                                               #
# - compare_environments                                                             #
# - compare_species                                                                  #
# - read_dft_energies                                                                #
######################################################################################


# Function to generate list of neighbours for each atom ordered by distance in a given structure, which is either in free BC or orthorhombic
# the atom itself is also included in the list
# all coordinates are centred at zero
# WARNING: untested for free BC
def find_neighbours(posinp, rcut, alat=None, print_neighbours=False, write=True, read=True, filename='neighbours.txt'):

    read_neighbours = False    
    if read:
        read_neighbours, all_neighbours = read_reference_environments(filename)

    if read_neighbours:
        return all_neighbours

    # all neighbours of all atoms
    all_neighbours = []

    # loop over all atoms
    for i,posi in enumerate(posinp):
        dist_list = [0.0]

        speciesi = posi[0]
        if print_neighbours:
            print('Neighbours of atom '+speciesi+' '+str(i+1))

        # neighbours of this atom, first neighbour is the central atom itself
        neighbours = [{'species': speciesi, 'posinp': [0.0, 0.0, 0.0], 'distance': 0.0}]

        # loop over all atoms to find neighbours of posi
        for j,posj in enumerate(posinp):
            if i == j:
                continue

            # get distance between neighbour and central atom
            d = at_dist(posi[1:4], posj[1:4], alat)
 
            # species of the potential neighbour
            speciesj = posj[0]

            if speciesi+'-'+speciesj in rcut:
                rcut_spec = rcut[speciesi+'-'+speciesj]
            elif speciesj+'-'+speciesi in rcut:
                rcut_spec = rcut[speciesj+'-'+speciesi]
            else:
                rcut_spec = rcut['other']
           
            if d < rcut_spec:
                # if we are within the species-dependent cutoff, add this atom to environment atoms
                # re-centre around atom i
                posj_shifted = [0, 0, 0]

                for xyz in range(3):
                    x1 = posj[xyz + 1] - posi[xyz + 1]

                    # take into account periodic BC
                    if alat is not None:
                        x2 = posj[xyz + 1] - alat[xyz] - posi[xyz + 1]
                        x3 = posj[xyz + 1] + alat[xyz] - posi[xyz + 1]
                        if abs(x1) < abs(x2) and abs(x1) < abs(x3):
                            posj_shifted[xyz] = x1
                        elif abs(x2) < abs(x1) and  abs(x2) < abs(x3):
                            posj_shifted[xyz] = x2
                        else:
                            posj_shifted[xyz] = x3
                    else:
                        posj_shifted[xyz] = x1

                neighbours.append({'species': speciesj, 'posinp': [posj_shifted[0], posj_shifted[1], posj_shifted[2]],
                                   'distance': d})
                dist_list.append(d)

        # sort by distance
        dindices = sorted(range(len(dist_list)), key=lambda k: dist_list[k])
        neighbours_sort = []
        for d,dind in enumerate(dindices):
           neighbours_sort.append(neighbours[dind])
           if print_neighbours:
               print(neighbours_sort[-1]['distance'], neighbours_sort[-1]['species'], neighbours_sort[-1]['posinp'])
        if print_neighbours:
            print('')

        all_neighbours.append(neighbours_sort)

    if write:
        write_reference_environments(filename, all_neighbours)

    return all_neighbours


# Function to write a set of reference environments to file
def write_reference_environments(filename, environments):

    f = open(filename,"w+")

    # write total number of environments
    f.write(str(len(environments))+'\n')

    for e,env in enumerate(environments):
        # write environment number, number of atoms
        f.write(str(e)+' '+str(len(env))+'\n')
        # write species, positions, and distance from central atom
        for i,at in enumerate(env):
            f.write(at['species']+' '+str(at['posinp'][0])+' '+str(at['posinp'][1])+' '+str(at['posinp'][2])+' '+str(at['distance'])+'\n')

    f.close()


# Function to read in environments from a file
def read_reference_environments(filename):
    import os
    from os import path

    environments = []
    read_ok = False
    if path.exists(filename):
        read_ok = True
    else:
        return False, -1

    f = open(filename)
    data = f.readlines()
    nenv = int(data[0].split()[0])
    row = 1
    for e in range(nenv):
        enum = int(data[row].split()[0])
        nat = int(data[row].split()[1])
        row += 1
        env = []
        for a in range(nat):
            at = data[row].split()[0]
            x = float(data[row].split()[1])
            y = float(data[row].split()[2])
            z = float(data[row].split()[3])
            d = float(data[row].split()[4])
            row += 1
            env.append({'species': at, 'posinp': [x, y, z], 'distance': d})
        environments.append(env)

    return read_ok, environments


# Function to get distance between two atoms, for a system which is either in free BC or orthorhombic
def at_dist(at1, at2, alat=None):
    import numpy as np

    d = 0.0
    for xyz in range(3):
        dxyz = abs(at1[xyz] - at2[xyz])
        if alat is not None:
            dxyz = min(dxyz, abs(alat[xyz] + at1[xyz] - at2[xyz]),
                       abs(alat[xyz] + at2[xyz] - at1[xyz]))
        d += dxyz * dxyz
    d = np.sqrt(d)
    return d


# Function to decompose a structure into distinct environments, adding each new environment to a reference list of environments
# if separate_species is True, treat neighbours of different species as a distinct environment
def deconstruct_structure(environments, ref_environments_in, species_list, separate_species=True, print_envs=False,
                          reference_file='reference.txt', decomposition_file='decomposition.txt', read=True, write=True):
    import copy

    ref_environments = copy.deepcopy(ref_environments_in)

    # read the environments if they exist to save recalculating
    read_ref = False
    if read:
        read_decomp, decomp = read_decomposition(decomposition_file)
        if read_decomp:
            read_ref, ref_environments = read_reference_environments(reference_file) 

    # if we successfully read BOTH the reference environments and decomposition for this structure, we can skip calculating
    # this assumes we don't need the ordered decomposition just the number of instances of each environment, otherwise we would have to recalculate
    if read_ref and read_decomp:
        return decomp, ref_environments, None

    decomposition = {}
    ordered_decomposition = []

    # if reference environments already exist, but the decomposition hasn't yet been calculated, set it to zero
    for ir in range(len(ref_environments)):
        decomposition[ir] = 0

    # loop over environments for atoms in this structure
    for ie,environment in enumerate(environments):

        # if reference_environments is empty, the first environment is by definition distinct
        if len(ref_environments) == 0:
            ref_environments.append(environment)
            ref_ind = 0
            decomposition[ref_ind] = 1
            ordered_decomposition.append(ref_ind)
            if print_envs:
                print('dding reference environment ',ref_ind + 1,' associated with atom ', environment[0]['species'],' ',ie + 1, 'nat = ',len(environment))
                for envtmp in environment:
                    print(envtmp['distance'], envtmp['species'])
            continue

        # otherwise, compare current environment with all existing reference environments
        # if there is no match (below threshold) then again add to reference environments
        ref_ind = -1

        for ir,ref_environment in enumerate(ref_environments):
            same_env = compare_environments(environment, ref_environment, species_list)

            # if already the same environment, no need to check other environments
            if same_env:
                ref_ind = ir
                decomposition[ref_ind] += 1
                ordered_decomposition.append(ref_ind)
                if print_envs:
                    print('Reference environment ',ref_ind + 1,' detected for atom ', environment[0]['species'],' ',ie + 1)
                break

        # none of the reference environments are similar, so need to add it to list
        if ref_ind == -1:
            ref_environments.append(environment)
            ref_ind = len(ref_environments) - 1
            decomposition[ref_ind] = 1
            ordered_decomposition.append(ref_ind)
            if print_envs:
                print('Adding reference environment ',ref_ind + 1,' associated with atom ', environment[0]['species'],' ',ie + 1, 'nat = ',len(environment))
                for env_print in environment:                
                    print(env_print['species'], env_print['distance'], env_print['posinp'])

    # check how many distinct environments are in this structure
    num_env = 0
    for ir in range(len(decomposition)):
        if decomposition[ir] > 0:
            num_env += 1
    decomposition['num_environments'] = num_env

    # write the environments to file to save re-calculating in future
    if write:
        write_reference_environments(reference_file, ref_environments)
        write_decomposition(decomposition_file, decomposition)

    return decomposition, ref_environments, ordered_decomposition


# Function to write the decomposition of a given structure to file
def write_decomposition(filename, decomposition):

    f = open(filename,"w+")

    # write number of environments
    f.write(str(decomposition['num_environments'])+'\n')
    for d in range(len(decomposition) - 1):
        # write environment number, number of occurrences
        if decomposition[d] > 0:
            f.write(str(d)+' '+str(decomposition[d])+'\n')

    f.close()


# Function to read in environments from a .txt file
def read_decomposition(filename):
    import os
    from os import path

    decomposition = {}
    read_ok = False
    if path.exists(filename):
        read_ok = True
    else:
        return False, -1

    f = open(filename)
    data = f.readlines()
    decomposition['num_environments'] = int(data[0].split()[0])
    row = 1
    for d in range(decomposition['num_environments']):
        enum = int(data[row].split()[0])
        n = int(data[row].split()[1])
        decomposition[enum] = n
        row += 1

    return read_ok, decomposition


# Function to compare two environments to see if they match
def compare_environments(environment, ref_environment, species_list):

    # first check that number of neighbours and atom types are consistent
    if len(environment) != len(ref_environment):
        return False
    else:
        return compare_species(environment, ref_environment, species_list)


# Function to check if two different environments are compatible in terms of number of atoms of each species
# the total number of atoms is assumed to already be the same
def compare_species(env1, env2, species_list):

    match = True 

    env1_species = {}
    env2_species = {}
    for species in species_list:
        env1_species[species] = 0
        env2_species[species] = 0

    # first check central atom is the same
    if env1[0]['species'] != env2[0]['species']:
        return False

    for i in range(len(env1)):
        env1_species[env1[i]['species']] += 1
        env2_species[env2[i]['species']] += 1

    for species in species_list:
        if env1_species[species] != env2_species[species]:
            match = False
            break

    return match


# Function to read DFT energies from a file
def read_dft_energies(energy_filename, nstrucs):

    energies = []
    
    # read energies from text file
    energyf = open(energy_filename, "r")
    energies_in = energyf.readlines()
    print('Reading energies from', energy_filename)
    energyf.close()
    
    # the first line is a comment, so we should skip it
    for istruc in range(1, nstrucs+1):
        energies.append(float(energies_in[istruc].split()[0]))
    
    return energies
