######################################################################################
# This file contains routines for fitting the machine learning model:                #
# - reorder_structures                                                               #
# - split_data                                                                       #
# - write_model                                                                      #
# - read_model                                                                       #
# - get_predicted_energy                                                             #
######################################################################################


# Function to reorder structures so that those with new environments appear first
def reorder_structures(structure_order, decomposition):
    import copy 

    new_structure_order = []
    nstrucs = len(structure_order)
    remaining_structures = copy.deepcopy(structure_order)
    present_environments = []
    for istruc in structure_order:
        new = False
        for dec in decomposition[istruc]:
            if dec == 'num_environments':
                continue
            elif decomposition[istruc][dec] > 0:
                # only take new structures if they contain a new environment
                if dec not in present_environments:
                    present_environments.append(dec)
                    new = True
        if new:
            new_structure_order.append(istruc)
            remaining_structures.remove(istruc)

    # take all remaining structures in existing order 
    for istruc in remaining_structures:
        new_structure_order.append(istruc)
        
    return new_structure_order


# Function to split a dataset into training data (which includes all structures with "new" environments) and validation data
def split_data(nvalid, dft_energies, structure_order, decomposition, num_environments):
    import copy

    validation_set = {'structures': [], 'dft_energies': [], 'occurrences': []}
    training_set = {'structures': [], 'dft_energies': [], 'occurrences': []}

    # it makes no sense to try and predict the energy if a structure has a new environment
    # so first we check for such cases and move these structures to the training set
    present_environments = []
    nstrucs = len(structure_order)
    remaining_structures = copy.deepcopy(structure_order)
    for istruc in structure_order:
        new = False
        for dec in decomposition[istruc]:
            if dec == 'num_environments':
                continue
            elif decomposition[istruc][dec] > 0:
                # only take new structures if they contain a new environment
                if dec not in present_environments:
                    present_environments.append(dec)
                    new = True
        if new:
            training_set['structures'].append(istruc)
            remaining_structures.remove(istruc)

    num_new = len(training_set['structures'])

    # find the total number of structures we should have in the training set
    # this has to be at least as big as the number of structures with new environments
    ntrain = max(nstrucs - nvalid, num_new)

    # recalculate nvalid in case ntrain ended up being larger than expected
    nvalid = nstrucs - ntrain

    # fill the rest of the training structures in order
    for i in range(ntrain - num_new):
        istruc = remaining_structures[0]
        training_set['structures'].append(istruc)
        remaining_structures.remove(istruc)
    
    # now fill the validation set with what remains
    for i in range(nvalid):
        istruc = remaining_structures[0]
        validation_set['structures'].append(istruc)
        remaining_structures.remove(istruc)

    # just as a sanity check
    if len(remaining_structures) != 0:
       print('Error, something went wrong in splitting data')
       assert False

    # now we can get the DFT energies for each data set
    # and unpack the occurrences of each environment
    for istruc in training_set['structures']:
        training_set['dft_energies'].append(dft_energies[istruc])
        occurrences = []
        for ienv in range(num_environments):
            if ienv in decomposition[istruc]:
                occurrences.append(decomposition[istruc][ienv])
            else:
                occurrences.append(0)
        training_set['occurrences'].append(occurrences)

    for istruc in validation_set['structures']:
        validation_set['dft_energies'].append(dft_energies[istruc])
        occurrences = []
        for ienv in range(num_environments):
            if ienv in decomposition[istruc]:
                occurrences.append(decomposition[istruc][ienv])
            else:
                occurrences.append(0)
        validation_set['occurrences'].append(occurrences)

    return training_set, validation_set


# Function to fit DFT data to the model for a given training set size
def fit_model(ntrain, training_data, validation_data, alpha=0.1, error_warn=None, fit_intercept=True):
    import numpy as np
    from sklearn.linear_model import Ridge
    from sklearn.metrics import mean_squared_error

    nvalid = len(validation_data['structures'])

    model = Ridge(fit_intercept=fit_intercept, copy_X=True, alpha=alpha, normalize=False)

    model.fit(training_data['occurrences'][:ntrain], training_data['dft_energies'][:ntrain])

    # predicted values for the training and validation sets
    predictions_train = model.predict(training_data['occurrences'][:ntrain])
    predictions_valid = model.predict(validation_data['occurrences'])

    # calculate the mean and maximum absolute errors in the validation set in meV/atom
    mae = 0.0
    maxae = 0.0
    for p,pred in enumerate(predictions_valid):
        ae = abs(pred - validation_data['dft_energies'][p])
        mae += ae
        maxae = max(maxae, ae)
    mae /= nvalid

    rmse = np.sqrt(mean_squared_error(validation_data['dft_energies'], predictions_valid))

    # check for large errors and print a warning
    maxae_all = 0.0
    for p,pred in enumerate(predictions_train):
        ae = abs(pred - training_data['dft_energies'][p])
        maxae_all = max(maxae_all, ae)
        if error_warn is not None and ae > error_warn:
            print('Error for training structure '+str(training_data['structures'][p] + 1)+' = '+'{0:.3f}'.format(ae)+' eV')

    for p,pred in enumerate(predictions_valid):
        ae = abs(pred - validation_data['dft_energies'][p])
        maxae_all = max(maxae_all, ae)
        if error_warn is not None and ae > error_warn:
            print('Error for validation structure '+str(validation_data['structures'][p] + 1)+' = '+'{0:.3f}'.format(ae)+' eV')

    # return all relevant information
    model_data = {}
    model_data['ntrain'] = ntrain
    model_data['nvalid'] = nvalid
    model_data['model'] = model
    model_data['MAE'] = mae
    model_data['RMSE'] = rmse
    model_data['MaxAE'] = maxae
    model_data['MaxAE_all'] = maxae_all
    model_data['R2_train'] = model.score(training_data['occurrences'][:ntrain], training_data['dft_energies'][:ntrain])
    model_data['R2'] = model.score(validation_data['occurrences'], validation_data['dft_energies'])
    model_data['training_set'] = {'dft_energies': training_data['dft_energies'][:ntrain], 
                                  'ml_energies': predictions_train,
                                  'occurrences': training_data['occurrences'][:ntrain],
                                  'structures': training_data['structures'][:ntrain]}
    model_data['validation_set'] = {'dft_energies': validation_data['dft_energies'], 
                                   'ml_energies': predictions_valid,
                                   'occurrences': validation_data['occurrences'],
                                   'structures': validation_data['structures']}

    return model_data


# Function to write model to file
def write_model(filename, model_data):

    ff = open(filename, "w")

    ff.write(str(len(model_data['model'].coef_))+'\n')
    for coef in model_data['model'].coef_:
        ff.write(str(coef)+'\n')
    ff.write(str(model_data['model'].intercept_)+'\n')

    ff.close()


# Function to read model from file
def read_model(filename):

    ff = open(filename, "r")
    data = ff.readlines()

    ncoef = int(data[0].split()[0])
    coef = []
    for i in range(1, ncoef + 1):
        coef.append(float(data[i].split()[0]))
    intercept = float(data[ncoef + 1].split()[0])

    ff.close()

    return {'coeff': coef, 'intercept': intercept}


# Function to get predicted energy based on model coefficients which have been read from file
def get_predicted_energy(decomp, model_coeff):

    energy = model_coeff['intercept']
    for env in decomp:
        if env == 'num_environments':
            continue
        else:
            energy += decomp[env] * model_coeff['coeff'][env]

    return energy    
